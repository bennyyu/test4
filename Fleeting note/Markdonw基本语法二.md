#Markdonw基本语法二


Created:2021-12-2016:58
**一、更改字体大小、颜色、更改字体**

<font face="逐浪新宋">我是逐浪新宋</font>
<font face="逐浪圆体">我是逐浪圆体</font>
<font face="逐浪花体">我是逐浪花体</font>
<font face="逐浪像素字">我是逐浪像素字</font>
<font face="逐浪立楷">我是逐浪立楷</font>
<font color=red>我是红色</font>
<font color=#008000>我是绿色</font>
<font color=yellow>我是黄色</font>
<font color=Blue>我是蓝色</font>
<font color= #871F78>我是紫色</font>
<font color= #DCDCDC>我是浅灰色</font>
<font size=5>我是尺寸</font>
<font size=10>我是尺寸</font>
<font face="逐浪立楷" color=green size=10>我是逐浪立楷，绿色，尺寸为5</font>

**二、更改字体大小、颜色、更改字体**

<table><tr><td bgcolor=green>背景色yellow</td></tr></table>

**三、Markdown创建表格**

颜色名 | 十六进制颜色值 |  rgb颜色  
-|-|-
黑色（black) | 000000 | 	rgb(0, 0, 0) |
蓝色（blue） | 0000FF | rgb(0, 0, 255) |
灰色（grey） | 808080 | rgb(128, 128, 128) |
绿色（green） | 008000 | rgb(0, 128, 0) |
橙色（orange） | FFA500 | rgb(255, 165, 0) |
红色(red) | FF0000 | rgb(255, 0, 0) |
黄色（yellow） | FFFF00 | rgb(255, 255, 0) |


**四、设置图片大小**


<div align=center><img src="https://www.z01.com/Template/office/style/images/home_product_phone09.png" width="50%" height="50%"></div>


##References
1. https://zhuanlan.zhihu.com/p/139007418

