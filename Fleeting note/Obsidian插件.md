#Obsidian插件
Created:2021-12-2017:16

# 10款好用的Obsidian插件推荐
## Kanban
![](https://www.v1tx.com/wp-content/uploads/2021/08/20210803074822.webp)

## Calendar
![](https://www.v1tx.com/wp-content/uploads/2021/08/20210803075845.webp)

## Outline
![](https://www.v1tx.com/wp-content/uploads/2021/08/20210803082612-2048x1297.webp)

## Mind Map
![](https://www.v1tx.com/wp-content/uploads/2021/08/20210803083802-2048x1224.webp)

## Sliding Panes
![](https://www.v1tx.com/wp-content/uploads/2021/08/20210819144009.webp)

## Editor Syntax Highlight
![](https://www.v1tx.com/wp-content/uploads/2021/08/20210817143105.webp)

## Outliner
![](https://www.v1tx.com/wp-content/uploads/2021/08/20210819143553.webp)

## Checklist
![](https://www.v1tx.com/wp-content/uploads/2021/08/20210819144911.webp)

## Emoji Toolbar
![](https://www.v1tx.com/wp-content/uploads/2021/08/20210803084437.webp)

## Paste URL into selection
![](https://www.v1tx.com/wp-content/uploads/2021/08/20210803085130.webp)



##References
1. https://www.v1tx.com/post/best-obsidian-plugins/

