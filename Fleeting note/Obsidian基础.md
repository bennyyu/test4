#Obsidian基础
Created:2021-12-2009:27
-  [ ] Obsidian基础

# **如何快速上手 Zettelkasten 笔记工具 Obsidian**

# 一. Obsidian 的基本使用
## 新建仓库
   
   <img   src="https://cdn.sspai.com/editor/u_5b3wva6y/16046439513203.png?imageView2/2/w/1120/q/40/interlace/1/ignore-error/1" width="100%" height="100%" >
	
	

##  Obsidian基本布局

   <img   src="http://n.sinaimg.cn/sinakd20201115s/479/w2048h1631/20201115/0963-kcysmrv6024659.png" width="100%" height="100%" >
   

## 新建笔记

<img   src="https://cdn.sspai.com/editor/u_5b3wva6y/16046439513244.png?imageView2/2/w/1120/q/90/interlace/1/ignore-error/1" width="100%" height="100%" >


# 二. 通用设置
## 主题设置
<img   src="https://cdn.sspai.com/editor/u_5b3wva6y/16046439513269.jpg?imageView2/2/w/1120/q/90/interlace/1/ignore-error/1" width="100%" height="100%" >

##  附件存储设置

   <img   src="https://cdn.sspai.com/editor/u_5b3wva6y/16046439513281.png?imageView2/2/w/1120/q/90/interlace/1/ignore-error/1" width="100%" height="100%" >
   

## 标签管理
   <img   src="https://cdn.sspai.com/editor/u_5b3wva6y/16046439513307.png?imageView2/2/w/1120/q/90/interlace/1/ignore-error/1" width="100%" height="100%" >
   


## 编辑模式 VS 预览模式

   <img   src="https://cdn.sspai.com/editor/u_5b3wva6y/16046439513320.png?imageView2/2/w/1120/q/90/interlace/1/ignore-error/1" width="100%" height="100%" >

## 命令模式
   <img   src="https://cdn.sspai.com/editor/u_5b3wva6y/16046439513353.png?imageView2/2/w/1120/q/90/interlace/1/ignore-error/1" width="100%" height="100%" >	     
   

# 三. 工作区管理   

![](https://cdn.sspai.com/editor/u_5b3wva6y/16046439513374.png?imageView2/2/w/1120/q/90/interlace/1/ignore-error/1)
	
# 四. 模板输入
![](https://cdn.sspai.com/editor/u_5b3wva6y/16046439513430.png?imageView2/2/w/1120/q/90/interlace/1/ignore-error/1)



# 五. 超链接使用

-   双向链接：`[[Note Name]]`
-   话题引用：`[[Note Name #header]]`
-   别名引用：`[[Note Name |Alias]]`
-   嵌入引用：`！[[Note Name]]`
-   块引用：`[[Note Name ^]]`

## 超链接语法 - 双向链接
![](https://cdn.sspai.com/editor/u_5b3wva6y/16046439513496.png?imageView2/2/w/1120/q/90/interlace/1/ignore-error/1)

##  超链接语法-话题引用
![](https://cdn.sspai.com/editor/u_5b3wva6y/16046439513518.png?imageView2/2/w/1120/q/90/interlace/1/ignore-error/1)

## 超链接语法- 别名引用
![](https://cdn.sspai.com/editor/u_5b3wva6y/16046439513540.png?imageView2/2/w/1120/q/90/interlace/1/ignore-error/1)
## 超链接语法 - 嵌入引用
![](https://cdn.sspai.com/editor/u_5b3wva6y/16046439513552.png?imageView2/2/w/1120/q/90/interlace/1/ignore-error/1)
## 超链接语法 - 块引用
![](https://cdn.sspai.com/editor/u_5b3wva6y/16046439513563.png?imageView2/2/w/1120/q/90/interlace/1/ignore-error/1)




##References
1. https://sspai.com/post/63481