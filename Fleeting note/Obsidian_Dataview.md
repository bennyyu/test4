#Obsidian_Dataview 
Created:2021-12-2114:23
- [ ] Obsidian Dataview 

## 内部代码

```dataview  
table file.ctime as "Created Time"  
from ""  
sort file.ctime desc  
```

## 文件列表
```dataview  
list  
```

## 任务列表dataview
```dataview  
task  
from "Fleeting note"
```

## 任务列表dataviewjs
```dataviewjs  
dv.header(3, 'Ukulele Tasks');  
dv.taskList(dv.pages('#Obsidian').file.tasks  
.where(t => !t.completed));  
```

##References
1. https://medium.com/os-techblog/how-to-get-started-with-obsidian-dataview-and-dataviewjs-5d6b5733d4a4

