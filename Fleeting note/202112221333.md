---
aliases: ZK，Markdown折叠语法
tags: Note Markdown 折叠
---
# 表头：
时间: 2021-12-2213:33
标题： #Markdown折叠语法
原题： #Markdown折叠语法
任务：
- [ ] 202112221333

# 内容：
### 1）示例如下：

<details>
  <summary>折叠文本</summary>
  此处可书写文本
  嗯，是可以书写文本的
</details>


<details>
  <summary>折叠代码块</summary>
  <pre><code> 
     System.out.println("虽然可以折叠代码块");
     System.out.println("但是代码无法高亮");
  </code></pre>
</details>

<details>
  <summary>折叠代码块</summary>
  <pre><blockcode> 
     System.out.println("虽然可以折叠代码块");
     System.out.println("但是代码无法高亮");
  </blockcode></pre>
</details>


# 参考：
1. https://www.cnblogs.com/yinhaiping/articles/11097803.html

