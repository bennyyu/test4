#Obsidian_DayPlanners
Created:2021-12-2115:31
- [x] Obsidian Day Planners

# Day Planners——高效日清单

![](https://pic2.zhimg.com/80/v2-4b49c6fb355cccd0d2159972287a7f5d_1440w.jpg)

## Day Planners Mode

-   File mode

文件模式：在该模式下，所有的Day Planners文档会统一保存在自动建立起来的【Day Planners】文件夹下，并且会新建今日的Day Planner文档。

-   Command mode（推荐）

命令行模式：在该模式下，是可以在你当前的文档中，通过输入命令行来插入日计划。（**笔者觉得就这一点来说，远比File mode更加灵活，所以更加推荐**）

![](https://pic3.zhimg.com/v2-1b194a96d810d1ee6f90008f4555c1ae_r.jpg)

##References
1. https://zhuanlan.zhihu.com/p/403228404

