---
aliases: 柏拉图
tags: 柏拉图
---
# 表头：
时间: 2021-12-2508:09
标题： #柏拉图
原题： #柏拉图
任务：
- [ ] 202112250809

# 内容：
柏拉图（Plato，Πλατών， 公元前427年—公元前347年），是[古希腊](https://baike.baidu.com/item/%E5%8F%A4%E5%B8%8C%E8%85%8A/14206)伟大的[哲学家](https://baike.baidu.com/item/%E5%93%B2%E5%AD%A6%E5%AE%B6/3968)，也是整个[西方文化](https://baike.baidu.com/item/%E8%A5%BF%E6%96%B9%E6%96%87%E5%8C%96/9585132)中最伟大的[哲学家](https://baike.baidu.com/item/%E5%93%B2%E5%AD%A6%E5%AE%B6/3968)和[思想家](https://baike.baidu.com/item/%E6%80%9D%E6%83%B3%E5%AE%B6/1214475)之一。

柏拉图和[老师](https://baike.baidu.com/item/%E8%80%81%E5%B8%88/5533)[苏格拉底](https://baike.baidu.com/item/%E8%8B%8F%E6%A0%BC%E6%8B%89%E5%BA%95/12690)，学生[亚里士多德](https://baike.baidu.com/item/%E4%BA%9A%E9%87%8C%E5%A3%AB%E5%A4%9A%E5%BE%B7/26769)并称为[希腊三贤](https://baike.baidu.com/item/%E5%B8%8C%E8%85%8A%E4%B8%89%E8%B4%A4/10841418)。他创造或发展的[概念](https://baike.baidu.com/item/%E6%A6%82%E5%BF%B5/829047)包括：柏拉图思想、[柏拉图主义](https://baike.baidu.com/item/%E6%9F%8F%E6%8B%89%E5%9B%BE%E4%B8%BB%E4%B9%89/5511427)、[柏拉图式爱情](https://baike.baidu.com/item/%E6%9F%8F%E6%8B%89%E5%9B%BE%E5%BC%8F%E7%88%B1%E6%83%85/256394)等。柏拉图的主要作品为对话录，其中绝大部分都有苏格拉底出场。但学术界普遍认为，其中的苏格拉底形象并不完全是历史上真实存在的苏格拉底。

除了[荷马](https://baike.baidu.com/item/%E8%8D%B7%E9%A9%AC/84187)之外，柏拉图也受到许多在他之前的作家和思想家的影响，包括了[毕达哥拉斯](https://baike.baidu.com/item/%E6%AF%95%E8%BE%BE%E5%93%A5%E6%8B%89%E6%96%AF/328218)提出的“和谐”概念，以及[阿那克萨戈拉](https://baike.baidu.com/item/%E9%98%BF%E9%82%A3%E5%85%8B%E8%90%A8%E6%88%88%E6%8B%89/10417108)将心灵或理性作为判断任何事情正确性的根据；[巴门尼德](https://baike.baidu.com/item/%E5%B7%B4%E9%97%A8%E5%B0%BC%E5%BE%B7/2029108)提出的连结所有事物的理论也可能影响了柏拉图对于灵魂的概念。
## 人物生平
## 思想观点
柏拉图是西方[客观唯心主义](https://baike.baidu.com/item/%E5%AE%A2%E8%A7%82%E5%94%AF%E5%BF%83%E4%B8%BB%E4%B9%89)的创始人，其哲学体系[博大精深](https://baike.baidu.com/item/%E5%8D%9A%E5%A4%A7%E7%B2%BE%E6%B7%B1)，对其教学思想影响尤甚。
## 人物评价




# 参考：
1. https://baike.baidu.com/item/柏拉图/85471?fr=aladdin


