#Obsidian应用
Created:2021-12-2111:18

# **Obsidian是什么以及它能用来做什么**

# 一 写日记
![](https://cdn.sspai.com/2021/06/24/6838eea4cfe02501a219b55c8d245cb4.png?imageView2/2/w/1120/q/90/interlace/1/ignore-error/1)

# 二 用来写文章

# 三 流笔记

# 四 日计划
![](https://cdn.sspai.com/2021/06/24/d8a2dee0092c9bd4c32560a412e1f6a6.png?imageView2/2/w/1120/q/90/interlace/1/ignore-error/1)
Day Planner插件
# 五 思维导图
![](https://cdn.sspai.com/2021/06/24/d699ce2f513c8d8747514c1e17a0309e.png?imageView2/2/w/1120/q/90/interlace/1/ignore-error/1)
Mind Map 使用

# 六 看板
![](https://cdn.sspai.com/2021/06/24/29d8742125afc43ebce368e6fd885b2e.png)
kanban插件的使用效果

##References
1. https://sspai.com/post/67399

