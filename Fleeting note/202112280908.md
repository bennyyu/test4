---
aliases: 机器人研究员登上Nature封面
tags: 自动化 化学实验室 工业机器人 流水线
---
# 表头：
时间: 2021-12-2809:08
标题： #机器人研究员登上Nature封面
原题： #工作007，8天完成688次实验，独立发现催化剂：机器人研究员登上Nature封面
任务：
- [ ] 202112280908

# 内容：
![](https://picture.iczhiku.com/weixin/weixin15942734446201.png)

在化学实验室里，自动化技术正在兴起，但迄今为止我们所说的「自动化」还仅限于一些仪器和定制界面，让部分机械与设备协同工作。利物浦大学的 Andrew Cooper 等人最近使用汽车生产过程中流水线上的工业机器人在湿化学实验室中实现了和人类一样的工作能力。现在，机器人可以使用和人类一样的工具做实验了。

![](https://picture.iczhiku.com/weixin/weixin15942734446202.gif)

# 参考：
1. https://picture.iczhiku.com/weixin/message1594273444620.html

