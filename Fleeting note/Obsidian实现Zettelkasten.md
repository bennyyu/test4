#Obsidian实现Zettelkasten
Created:2021-12-2208:48
- [x] Obsidian实现Zettelkasten
# 用Obsidian实现Zettelkasten看这一篇就够了（上）
![](https://pic1.zhimg.com/v2-837d388b7d065aab1cd45ea9829500c7_1440w.jpg?source=172ae18b)

## 一. 动手实践Zettelkasten

第一，**建立一套属于自己的滑箱系统**。
第二，滑箱只是工具，**必不可少的还需要配上一套Workflow工作流，这套工作流能够帮助你在认知上积少成多，不断丰富滑箱内容逐步达到质量的临界点，触发认知涌现**。
第三，**自下而上、刻意练习，逐渐内化成自觉的行为习惯**：**阅读、理解、链接、思考、涌现**.....如此迭代往复，螺旋上升，实现质变。


### 1、在Obsidian里构建滑箱系统
第一步，先做卡片盒子。
我们的滑箱结构包括三个卡片盒子：FleetingBox（灵感盒子）、LiteratureBox（文献盒子）、PermanentBox（永久盒子）。![](https://pic4.zhimg.com/80/v2-1dd5dff21dc7e9b130c00fdd3cffa043_1440w.jpg)
第二步，做Zettelkasten卡片：
**标题**,**链接**,**标签**,**给卡片多起几个别名**![](https://pic3.zhimg.com/80/v2-0c0ea8b9b4964eb05273a51bbf548b1a_1440w.jpg)
第三步，持续优化标签、目录树和索引
[Obsidian&Zettelkasten&LYT系列文章的总目录](https://zhuanlan.zhihu.com/p/360569582)

### 2、在Obsidian里实现Workflow
知识迭代的Workflow
![|600](https://pic2.zhimg.com/80/v2-4bcd7b1b76257dae555d2ef4e1bb4c39_1440w.jpg)
每日事项的Workflow
![|600](https://pic2.zhimg.com/80/v2-3e26bace0b797b7a4267609569856a21_1440w.jpg)
##References
1. https://zhuanlan.zhihu.com/p/360599265


