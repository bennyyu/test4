---
aliases: %(citekey)
date:
tags: #
---

{{title}}
{{abstractNote}}
<cite>{{author}}<cite>

 {{date}}

 {{tags}}

 {{URL}}

 {{DOI}}

 {{localLibrary}}

 {{pdfAttachments}}

***
{{notes}}