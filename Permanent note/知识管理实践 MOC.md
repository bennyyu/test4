---
aliases: 知识管理实践
tags: 知识管理 MOC 
---
# 表头：
时间: 2021-12-2213:04
议题： 知识管理实践 MOC
任务：
- [ ] 知识管理实践 MOC

# 内容：

## 一.  知识管理哲学（Why)
自然中的事情，大致分为经验主义的代表[[Fleeting note/202112231004|约翰·洛克]]，理性主义的代表[[Fleeting note/202112231036|勒内·笛卡尔]] ,最后逻辑实证主义的代表[[Fleeting note/202112231053|大卫·休谟]] 怀疑两者。他用叉子[[Fleeting note/202112231106|休谟的叉子]]形象的区分两者，列出各自优缺点。在经验主义和理性主义之争的历史上，还有一位试图调和双方分歧的人物，就是德国古典主义哲学的奠基人康德[[Fleeting note/202112231130|伊曼努尔·康德]]。康德原本是理性主义的哲学家，但他看到休谟的书，有一种猛然惊醒梦中人的感觉，花了很多年时间修正自己的哲学思想，来回应休谟的思想。


## 二. 知识管理方法 (What)
目前知识采用用文件记录，目录管理。检索方法也分为基于文件名的检索和文件内容的检索，主要代表性的是MOC方法和PARA([[Fleeting note/202112221416#PARA的理念]])方法。[[Fleeting note/202112221236|知识管理方法]]

## 三. 全文检索+时间戳+MOC（How）

 1) ZK卡片制作：
 标题，  链接 ， 标签 ， 给卡片多起几个别名[[Fleeting note/202112220955|ZK卡片]]

 2) ZK卡片盒制作:
  灵感盒子[[Fleeting note/202112221036#FleetingBox]]：
  灵感盒子文件内容来源于零散的想法，随时随地记录。形式上通常使用ZK模版文件[[Fleeting note/202112220955|ZK卡片]]。[[Fleeting note/202112221036|ZK卡片盒]]
  文献盒子[[Fleeting note/202112221036#LiteraturBox]]：
  文献盒子文件内容来源于文献阅读，文献管理工具通常用Zotero，文件形式通常以为Mdnotes Default Template.md为模版。来源于 [[Fleeting note/202112241021|Mdnotes插件]] ，也称呼文献卡片[[Fleeting note/202112240820|zotero与obsidian配合]],[[Fleeting note/202112232156|Obsidian结合Zotero]]。
  永久盒子[[Fleeting note/202112221036#PermanentBox]]：
  项目盒子 ：
  
 3) MOC工作流程：
 Assembling Phrase 组装阶段，Colliding Phrase 冲突阶段，Unifying Phrase 统一阶段[[Fleeting note/202112221149|MOC工作流过程]]
 
 3) MOC工作事务：
 知识迭代的Workflow，每日事项的Workflow[[Fleeting note/202112221023|MOC工作流事务]]
 

## 四. 案例

## 五. 总结







