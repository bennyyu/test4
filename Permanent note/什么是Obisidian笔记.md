#什么是Obisidian笔记
Created:2021-12-2111:07

## 一. 基础 (What)
1) **基本使用**
 新建仓库，基本布局，新建笔记,工作区管理,模板输入
2) 通用设置
主题设置，附件存储设置，标签管理，编辑模式 VS 预览模式，命令模式
3) 超链接使用
 双向链接,话题引用, 别名引用,嵌入引用,块引用
[[Fleeting note/Obsidian基础]]
## 二. 插件
Kanban 看板
Calendar 日历
Outline 大纲
Mind Map 思维导图
Sliding Panes 滑窗
Editor Syntax Highlight 编辑格式高亮
Outliner
Checklist 任务
Emoji Toolbar
Paste URL into selection
 [[Fleeting note/Obsidian插件]]
Dataview
[[Fleeting note/Obsidian_Dataview]]
DayPlanners
[[Fleeting note/Obsidian_DayPlanners]]
Excalidraw
[[Fleeting note/Obsidian_Excalidraw]]

 ## 三. 应用
 写日记，写文章，流笔记，日计划， 思维导图，看板
 [[Fleeting note/Obsidian应用]]

## 四. 过程实践（How）
1)  先用自己的话写笔记
2)   迫使自己建立笔记链接
3)   形成网状结构
4)   使用 Anki 间隔重复增强复现几率
5)   最后也最重要的是：从中找到一个写作的主题
[[Fleeting note/Obsidian做笔记#Obsidian流程]]

## 五. 选择原因（Why）
  1) 为什么要用笔记
      人脑内部逻辑思维（提高认知），需要借助外部设备（物质）的辅助。[[Fleeting note/Obsidian做笔记#提高认知能力的外部辅助器]]
  3) 为什么要用Obsidian
      1)支持多系统
	  2)Markdown语法简单
	  3)支持链接，标签
	  4)插件丰富
	  [[Fleeting note/Obsidian做笔记#Obsidian介绍]]
	  








