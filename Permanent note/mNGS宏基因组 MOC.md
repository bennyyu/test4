---
aliases: mNGS,宏基因组
tags: mNGS 宏基因组
---
# 表头：
时间: 2021-12-2419:13
议题： mNGS宏基因组 MOC
任务：
- [ ] mNGS宏基因组 MOC

# 内容：
 ## 简介 
鸟枪法宏基因组 和  靶向测序(tNGS)
[[202112241905|高通量测序技术打开病原体诊断的钥匙]]

### 靶向测序(tNGS)
 [[zhangMicrobialAlterationSmall2020-zotero|短肠综合征婴儿小肠造口流出物和结肠粪便的微生物改变]]

 #### _DNA提取，定量和扩增_
 [[zhangMicrobialAlterationSmall2020 - Extracted Annotations (20211224 下午52136)#DNA提取，定量和扩增：]]
 ####  _miseq测序_
 [[zhangMicrobialAlterationSmall2020 - Extracted Annotations (20211224 下午52136)#_miseq测序：]]

 ####  _数据处理1_
 [[zhangMicrobialAlterationSmall2020 - Extracted Annotations (20211224 下午52136)#_数据处理1]]
 #### _数据处理2_
 [[zhangMicrobialAlterationSmall2020 - Extracted Annotations (20211224 下午52136)#_数据处理2]]
 #### _功能预测_
 [[zhangMicrobialAlterationSmall2020 - Extracted Annotations (20211224 下午52136)#功能预测：]]
 #### _统计分析_
 [[zhangMicrobialAlterationSmall2020 - Extracted Annotations (20211224 下午52136)#统计分析：]]
### 鸟枪法宏基因组

#### 分桶
[[vilskerGenomeDetectiveAutomated2019 - Extracted Annotations (20211227 上午91758)#_通过比对蛋白库数据分桶：]]
#### 组装

[[vilskerGenomeDetectiveAutomated2019 - Extracted Annotations (20211227 上午91758)#_数据组装，比对核酸库分群：]]
#### 重叠群
[[vilskerGenomeDetectiveAutomated2019 - Extracted Annotations (20211227 上午91758)#_重叠群连接，提高准确性和灵敏度：]]
#### 报告
[[vilskerGenomeDetectiveAutomated2019 - Extracted Annotations (20211227 上午91758)#_报告生成：]]







 

 




