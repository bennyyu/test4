#Markdown

## 一. 对象基础
标题，字体，引用，图片，超链接，列表，表格，代码，流程图
[[Fleeting note/Markdown基本语法一]]

## 二. 对象属性
大小，颜色，更改字体，对齐
[[Fleeting note/Markdonw基本语法二]]

## 三.  高级（多对象）
支持的 HTML 元素，转义，公式，流程图
[[Fleeting note/Markdown高级技巧]]