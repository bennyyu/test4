---
aliases: 从高通量测序数据中识别病毒的自动化系统
tags: mNGS 宏基因组 鸟枪法
---
#  Genome Detective：从高通量测序数据中识别病毒的自动化系统
Genome Detective: an automated system for virus identification from high-throughput sequencing data

## Metadata

* Item Type: [[Article]]
* Authors: [[Michael Vilsker]], [[Yumna Moosa]], [[Sam Nooij]], [[Vagner Fonseca]], [[Yoika Ghysens]], [[Korneel Dumon]], [[Raf Pauwels]], [[Luiz Carlos Alcantara]], [[Ewout Vanden Eynden]], [[Anne-Mieke Vandamme]], [[Koen Deforche]], [[Tulio de Oliveira]]
* Date: [[2019-03-01]]
* Date Added: [[2021-11-27]]
* URL: [https://academic.oup.com/bioinformatics/article/35/5/871/5075035](https://academic.oup.com/bioinformatics/article/35/5/871/5075035)
* DOI: [10.1093/bioinformatics/bty695](https://doi.org/10.1093/bioinformatics/bty695)
* Cite key: vilskerGenomeDetectiveAutomated2019
* Topics: [[mNGS宏基因组]]
, #zotero, #literature-notes, #reference
* PDF Attachments
	- [Vilsker 等。 - 2019 - Genome Detective an automated system for virus id.pdf](zotero://open-pdf/library/items/KRWJEUX2)

## 摘要：
Genome Detective 是一款易于使用的基于 Web 的软件应用程序，可快速准确地组装病毒基因组。该应用程序使用了一种新的比对方法，该方法通过结合氨基酸和核苷酸评分，通过基于参考的_de novo_ contigs连接来构建基因组。该软件使用合成数据集进行了优化，以代表病毒基因组的巨大多样性。然后使用数百种病毒的下一代测序数据验证了该应用程序。用户时间最少，仅限于上传数据所需的时间。Abstract

Summary: Genome Detective is an easy to use web-based software application that assembles the genomes of viruses quickly and accurately. The application uses a novel alignment method that constructs genomes by reference-based linking of de novo contigs by combining amino-acids and nucleotide scores. The software was optimized using synthetic datasets to represent the great diversity of virus genomes. The application was then validated with next generation sequencing data of hundreds of viruses. User time is minimal and it is limited to the time required to upload the data.


##  Zotero links
* [Local library](zotero://select/items/1_HFTYKQSF)
* [Cloud library](http://zotero.org/users/8881316/items/HFTYKQSF)

## Highlights and Annotations

- [[vilskerGenomeDetectiveAutomated2019 - Extracted Annotations (20211227 上午91758)]]