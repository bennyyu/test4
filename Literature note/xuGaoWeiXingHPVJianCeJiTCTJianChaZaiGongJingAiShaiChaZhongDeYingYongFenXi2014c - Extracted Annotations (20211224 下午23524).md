---
aliases: 高危型HPV检测及TCT检查在宫颈癌筛查中的应用分析note
tags: HPV检测 TCT检测
---
**Extracted Annotations (2021/12/24 下午2:35:24)**

"NCCN 2012 《》 HPV 1 30 ～ 65 。" ([许 and 徐 2014:947](zotero://open-pdf/library/items/QY46QVFD?page=2))
 ## 2012美国NCCN作为指南发布 ([note on p.947](zotero://open-pdf/library/items/QY46QVFD?page=2))_
  美国国立综合癌症网络(NCCN)2012公布的《宫颈癌筛查临床实验指南》已联合细的南HPV胞学检查作岁女性宫颈癌的筛查手段

"HPV 8 HPV HPV16、8、1、 33、5、2、6、8。HPV DNA PCＲ" ([许 and 徐 2014:947](zotero://open-pdf/library/items/QY46QVFD?page=2))

_2 #什么是HPV检测 ([note on p.947](zotero://open-pdf/library/items/QY46QVFD?page=2))_

"TCT 。TBS NILM ASC 、 SIL SCC AGC 。ASC US ASC- H ASCSIL LSIL HSIL" ([许 and 徐 2014:947](zotero://open-pdf/library/items/QY46QVFD?page=2))

_3 #什么是TCT检测 ([note on p.947](zotero://open-pdf/library/items/QY46QVFD?page=2))_

"3" ([许 and 徐 2014:948](zotero://open-pdf/library/items/QY46QVFD?page=3))

_4 #宫颈刮片被TCT淘汰 ([note on p.948](zotero://open-pdf/library/items/QY46QVFD?page=3))_

"H PV TCT 5 8 。 HPV" ([许 and 徐 2014:949](zotero://open-pdf/library/items/QY46QVFD?page=4))

_5 #HPV和TCT检测联合，优势互补 ([note on p.949](zotero://open-pdf/library/items/QY46QVFD?page=4))_