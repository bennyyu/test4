---
aliases: 高危型HPV检测及TCT检查在宫颈癌筛查中的应用分析
tags: TCT检测 HPV检测
---
# 高危型HPV检测及TCT检查在宫颈癌筛查中的应用分析

## Metadata

* Item Type: [[Article]]
* Authors: [[剑利 许]], [[克惠 徐]]
* Date: [[2014]]
* Date Added: [[2021-11-24]]
* URL: [https://kns.cnki.net/KCMS/detail/detail.aspx?dbcode=CJFD&dbname=CJFDLAST2015&filename=SFCZ201412026&v=](https://kns.cnki.net/KCMS/detail/detail.aspx?dbcode=CJFD&dbname=CJFDLAST2015&filename=SFCZ201412026&v=)
* Cite key: xuGaoWeiXingHPVJianCeJiTCTJianChaZaiGongJingAiShaiChaZhongDeYingYongFenXi2014c
* Tags: #高危型HPV, #宫颈癌, #宫颈癌筛查, #液基薄层细胞学-The-screening-of-cervical-carcinoma, #High-risk-human-papillomavirus, #HPV, #TCT, #Thin-prep-liquid-based-cytology-test, #zotero, #literature-notes, #reference
* PDF Attachments
	- [许_徐_2014_高危型HPV检测及TCT检查在宫颈癌筛查中的应用分析.pdf](zotero://open-pdf/library/items/QY46QVFD)

## Abstract

目的:探讨高危型人乳头瘤病毒(HPV)、液基薄层细胞学(TCT)技术在宫颈癌筛查中的价值。方法:回顾性分析具有病理检查结果的184例患者行高危型HPV检测及TCT检查的数据,以病理检查结果作为诊断金标准(CINⅡ及以上为阳性),分别计算HPV、TCT两种检测方法在宫颈癌筛查中的灵敏度、特异度、阳性预测值、阴性预测值及准确度,同时比较二者有无统计学差异性。分析73例高危型HPV病毒载量与宫颈病变程度的相关性。结果:184例患者中63例病理检查结果为阳性,包括CINⅡ5例、CINⅢ28例、宫颈癌30例。HPV、TCT筛查的灵敏度分别为100.00%、11.11%,差异有统计学意义(P<0.01);特异度分别为0、94.21%,差异有统计学意义(P<0.01);阳性预测值分别为34.23%、50.00%,差异无统计学意义(P>0.05);阴性预测值分别为0、67.06%,差异有统计学意义(P<0.01);准确度分别为34.23%、65.76%,差异有统计学意义(P<0.01)。高危型HPV病毒载量与宫颈病变程度呈正相关关系(r=0.389,P<0.01)。结论:HPV检测具有灵敏度高、但特异度及准确度低的特点,TCT检查则具有灵敏度低、但特异度及准确度高的特点,建议二者应联合应用于宫颈癌的筛查。高危型HPV病毒载量可作为肿瘤标志物用于宫颈病变的治疗随访。


##  Zotero links
* [Local library](zotero://select/items/1_BCFF4J3W)
* [Cloud library](http://zotero.org/users/8881316/items/BCFF4J3W)

## Highlights and Annotations

- [[xuGaoWeiXingHPVJianCeJiTCTJianChaZaiGongJingAiShaiChaZhongDeYingYongFenXi2014c - Extracted Annotations (20211224 下午23524)]]