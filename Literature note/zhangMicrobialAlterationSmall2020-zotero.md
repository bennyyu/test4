---
aliases: 短肠综合征婴儿小肠造口流出物和结肠粪便的微生物改变
tags: 肠道菌群 短肠综合征 小肠造口流出物 mNGS 宏基因组 靶向法
---
# Microbial alteration of small bowel stoma effluents and colonic feces in infants with short bowel syndrome
 # 短肠综合征婴儿小肠造口流出物和结肠粪便的微生物改变

## Metadata

* Item Type: [[Article]]
* Authors: [[Tian Zhang]], [[Ying Wang]], [[Weihui Yan]], [[Lina Lu]], [[Yijing Tao]], [[Jie Jia]], [[Wei Cai]]
* Date: [[07/2020]]
* Date Added: [[2021-11-24]]
* URL: [https://linkinghub.elsevier.com/retrieve/pii/S0022346819305111](https://linkinghub.elsevier.com/retrieve/pii/S0022346819305111)
* DOI: [10.1016/j.jpedsurg.2019.08.004](https://doi.org/10.1016/j.jpedsurg.2019.08.004)
* Cite key: zhangMicrobialAlterationSmall2020
* Topics: [[NGS]]
* Tags: #宏基因组, #mNGS, #zotero, #literature-notes, #reference, 
* PDF Attachments
	- [Zhang 等。 - 2020 - Microbial alteration of small bowel stoma effluent.pdf](zotero://open-pdf/library/items/276Q734B)

## Abstract

Background and aim: Studies about differences in microbial communities between the small intestine and colon in infants with short bowel syndrome (SBS) are rare. We aimed to characterize the bacterial diversity of small bowel stoma efﬂuents and feces of SBS infants.
Methods: Seven SBS infants were enrolled in this study and provided two samples (one from the stoma and the other from the anus) each. Eleven age-matched healthy controls were recruited to provide one fecal sample each. 16S rRNA gene MiSeq sequencing was conducted to characterize the microbiota diversity and composition.
Results: The bacterial diversity of the stoma efﬂuents was signiﬁcantly higher than that in the feces of SBS infants. Proteobacteria dominated in both the stoma efﬂuents and colonic. Acinetobacter (P = 0.004), Klebsiella (P = 0.015), Citrobacter (P = 0.019), and Lactobacillus (P = 0.030) were more abundant in stoma efﬂuents compared to feces of SBS patients, while Bacteroidetes, Biﬁdobacterium and Veillonella were less abundant in stoma efﬂuents. Signiﬁcantly higher levels of Proteobacteria, Enterococcus and lower levels of Blautia, Collinsella, Faecalibacterium, Veillonella were present in the fecal samples of SBS patients than those in the healthy controls. Kyoto Encyclopedia of Genes and Genomes pathways related to metabolism and membrane function were depleted in SBS patients.
Conclusions: The predominant intestinal bacterial groups were different in SBS children before and after the ﬁstula closure. Fecal samples of SBS patients featured overabundant Proteobacteria and less SCFA producing bacteria. Depleted functional proﬁles of the microbiome were found in fecal samples of SBS patients.

**背景和目的：** 关于短肠综合征 (SBS) 婴儿小肠和结肠微生物群落差异的研究很少见。我们旨在表征 SBS 婴儿小肠造口流出物和粪便的细菌多样性。

**方法：** 七名 SBS 婴儿参加了这项研究，并提供了两个样本（一个来自造口，另一个来自肛门）。招募了 11 名年龄匹配的健康对照，每人提供一份粪便样本。进行 16S rRNA 基因 MiSeq 测序以表征微生物群的多样性和组成。

**结果：** 造口流出物的细菌多样性显着高于SBS婴儿粪便中的细菌多样性。Proteobacteria 在造口流出物和结肠中均占主导地位。与 SBS 患者的粪便相比，不动杆菌 (P = 0.004)、克雷伯氏菌 (P = 0.015)、柠檬酸杆菌 (P = 0.019) 和乳杆菌 (P = 0.030) 在造口流出物中的含量高于 SBS 患者的粪便，而拟杆菌、双歧杆菌和丝状杆菌的含量较少在造口流出物中。SBS 患者的粪便样本中变形杆菌、肠球菌的含量显着高于健康对照组，而布劳氏菌、柯林斯氏菌、粪杆菌、韦永氏菌的含量显着低于健康对照组。京都基因和基因组百科全书与代谢和膜功能相关的途径在 SBS 患者中耗尽。

**结论：** SBS患儿瘘管闭合前后主要肠道菌群不同。SBS 患者的粪便样本具有过多的变形菌和较少的产生 SCFA 的细菌。在 SBS 患者的粪便样本中发现了微生物组的耗尽功能谱。


##  Zotero links
* [Local library](zotero://select/items/1_XRQEGLWI)
* [Cloud library](http://zotero.org/users/8881316/items/XRQEGLWI)

## Highlights and Annotations

- [[zhangMicrobialAlterationSmall2020 - Extracted Annotations (20211224 下午52136)]]