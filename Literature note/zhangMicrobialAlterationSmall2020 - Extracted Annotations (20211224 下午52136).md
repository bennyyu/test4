---
aliases: 短肠综合征婴儿小肠造口流出物和结肠粪便的微生物改变note
tags: 肠道菌群 短肠综合征 小肠造口流出物 mNGS 宏基因组 靶向法
---
**Extracted Annotations (2021/12/24 下午5:21:36)**

"Microbial DNA was extracted from each fecal sample using the QIAampFast DNA Stool Mini Kit (Qiagen, Hilden, Germany). The concentration of extracted DNA was measured using a Nano-Drop 2000 spectrophotometer (Thermo Scientific, USA). PCR amplification of the 16S rRNA gene was performed according to the literature [17] using PCR primers specific for the 515-806 (V3-V4) regions. The PCR assays were carried out in triplicate as follows: 20-ml reaction solutions with 10 ng of template DNA, 4μl of PCR reaction buffer, 0.4 mM of each primer, 2.5 mM of deoxyribonucleotide triphosphate (dNTPs), and 0.5 U of TransStartFastPfu DNA polymerase (TransGen Biotech). The conditions of PCR were as follows: 95 °C for 4 min, followed by 27 cycles of 95 °C for 30 s, 55 °C for 30 s, 72 °C for 45 s, and afinal extension of 72 °C for 10 min." ([Zhang et al 2020:1367](zotero://open-pdf/library/items/276Q734B?page=2))
## DNA提取，定量和扩增：
  使用QIAampFast DNA Stool Mini Kit（Qiagen，德国希尔顿）从每个粪便样本中提取微生物DNA。提取的DNA的浓度使用Nano-Drop 2000分光光度计（Thermo Scientific, USA）测量提取的DNA的浓度。PCR扩增的16S rRNA基因的PCR扩增是根据文献[17]进行的。  
  使用<font color=red>515-806（V3-V4）区域</font>的特异性PCR引物。PCR检测  
  一式三份，具体如下。20毫升的反应液中含有10纳克模板DNA，4微升PCR反应缓冲液，0.4毫摩尔的每个引物，2.5mM的脱氧核苷酸三磷酸酯（dNTPs），以及0.5U的TransStart®。0.5 U TransStartFastPfu DNA聚合酶（TransGen Biotech）。PCR的条件如下PCR的条件如下。95℃，4分钟，然后是27个循环，95℃30秒，55℃30秒，72℃45秒，最后延伸至  
 最后延长72℃10分钟。  
([note on p.1367](zotero://open-pdf/library/items/276Q734B?page=2))_

"Amplicons were extracted from 2% agarose gels and purified using the AxyPrep DNA Gel Extraction Kit (Axygen Biosciences, Union City, CA, USA) and quantified using QuantiFluo-ST (Promega, Madison, WI, USA). Purified amplicons were pooled in equimolar and paired-end sequences (2 × 250) on an Illumina MiSeq platform." ([Zhang et al 2020:1367](zotero://open-pdf/library/items/276Q734B?page=2))

## _miseq测序： 
从2%的琼脂糖凝胶中提取扩增子，并使用以下方法纯化AxyPrep DNA凝胶提取试剂盒（Axygen Biosciences, Union City,CA, USA），并使用QuantiFluo-ST（Promega, Madison, WI,美国）进行量化。纯化的扩增子被汇集在等摩尔和成对的序列中  
(2 × 250) 在Illumina MiSeq平台上。 ([note on p.1367](zotero://open-pdf/library/items/276Q734B?page=2))_

 "Raw Fastqfiles were demultiplexed, quality-filtered using QIIME (version 1.17) with the following criteria: (1) 300 bp reads were truncated at any site receiving an average quality score 20 over a 50 bp b sliding window, discarding the truncated reads that were shorter than" ([Zhang et al 2020:1367](zotero://open-pdf/library/items/276Q734B?page=2))

## _数据处理1: 
  原始Fastq文件去掉多重扩增子，用QIIME进行质量过滤(1.17版）进行质量过滤，标准如下。(1) 300bp读数被截断组成(1) 在任何部位的连续50bp以上平均质量得分小于20的舍弃，丢弃短于50bp的截断read。 ([note on p.1367](zotero://open-pdf/library/items/276Q734B?page=2))_

  "exact barcode matching, two nucleotide mismatch in primer matching, and reads containing ambiguous characters were removed; and (3) only sequences that overlapped longer than 10 bp were assembled according to their overlap sequence. Reads that could not be assembled were discarded. Operational taxonomic units (OTUs) were clustered with 97% similarity cutoff using UPARSE version 7.1 (http:// drive5.com/ uparse/), and chimeric sequences were identified and removed using UCHIME (http://drive5.com/index.htm). The phylogenetic affiliation of each 16S rRNA gene sequence was analyzed by RDP Classifier (http:// rdp.cme.msu.edu/) against the SILVA (SSU117/119)16S rRNA database." ([Zhang et al 2020:1368](zotero://open-pdf/library/items/276Q734B?page=3))

## 数据处理2： 
(2)准确的条形码匹配，引物中的两个核苷酸错配和含有模糊字符的读数被删除。(3) 只对重叠超过10bp的序列进行组装组成  
根据它们的重叠序列进行组合。不能组装的丢弃。使用UPARSE7.1版（http://drive5.com/ uparse/），以97%的相似度为界限，对操作性分类单位（OTU）进行聚类。并对嵌合序列进行识别和重新去除使用UCHIME（http://drive5.com/index.htm）。每个16S rRNA的系统发育每条16S rRNA基因序列的系统发育关系用RDP分类法进行分析。判断(http:// rdp.cme.msu.edu/)对SILVA(SSU117/119)16S  
rRNA数据库。 ([note on p.1368](zotero://open-pdf/library/items/276Q734B?page=3))_

"Bacterial functions were predicted by the Phylogenetic Investigation of Communities by Reconstruction of Unobserved States (PICRUSt) package [18]. Briefly, the procedure was carried out as follows: OTU picking against the Greengenes database (V.13.5) was performed using Mothur. At the same time, a BIOM table compatible with the PICRUSt program was generated. Metagenome prediction was made with the OTU table after normalizing for 16S copy number. The predicted functional gene was annotated using the Kyoto Encyclopedia of Genes and Genomes (KEGG) pathway [19]." ([Zhang et al 2020:1368](zotero://open-pdf/library/items/276Q734B?page=3))
## 功能预测：
细菌功能预测由系统发育调查（Phylogenetic Investigation of Communities by Reconstruction of Unobserved States (PICRUSt) ）包预测。简而言之，该程序是这样进行的：用Mothur挑选OTU对照Greengenes数据库（V.13.5）。同时，生成一个与PICRUSt程序兼容的BIOM表由PICRUSt程序生成。元基因组预测在对16S拷贝数进行归一化后，用OTU表进行预测。预测的功能基因是用京都百科全书中的基因和基因组（KEGG）途径[19]注释。 ([note on p.1368](zotero://open-pdf/library/items/276Q734B?page=3))_

"Alpha-diversity parameters, including Sobs, Chao, Simpson, and Shannon, and a sequencing depth index (Good's coverage), were calculated using the Mothur software [20]. Beta diversity measurements were calculated as described [21], and partial least squares discriminant analysis (PLS-DA) based on OTU compositions were determined to evaluate the inter-group difference using the Vegan package of R software (Vienna, Austria). The Wilcoxon rank-sum test was performed to evaluate the differences in microbiota distribution and abundance of level 2 functional pathways between two groups (feces of SBS infants vs. feces of healthy controls and stoma effluents vs. colonic feces for SBS infants). Spearman's test was performed to analyze the correlation between proportions of bacteria in stoma effluents with indices of intestinal adaptation. The statistical analysis described above was performed using SPSS version 22 (Chicago, IL, USA). P 0.05 was considb ered statistically significant." ([Zhang et al 2020:1368](zotero://open-pdf/library/items/276Q734B?page=3))

## 统计分析：
使用Mothur软件[20]计算阿尔法多样性参数，包括Sobs、Chao、Simpson和Shannon，以及测序深度指数（Good的覆盖率）。β多样性参数的计算方法如前所述[21]，基于OTU组成的偏最小二乘判别分析（PLS-DA）被确定，以评估使用R软件的Vegan包（Vienna, Austria）评估组间差异。基于OTU组成的偏最小二乘判别分析（PLS-DA）被确定，以评估 使用R软件的Vegan包（Vienna, Austria）评估组间差异。进行Wilcoxon秩和检验，以评估两组（SBS婴儿的粪便与健康对照组的粪便和造口组的粪便）之间的微生物群分布和2级功能途径的丰度差异。 健康对照组的粪便，以及SBS婴儿的造口流出物与结肠粪便的差异。婴儿）。 进行Spearman's测试来分析两组之间的相关性。 之间的相关性。  
细菌在造口流出物中的比例与肠道适应指数的相关性。  
肠道适应性。上述的统计分析是按照 形成 使用SPSS第22版（Chicago, IL, USA）。P<0.05被认为是 b 被视为 有统计学意义 ([note on p.1368](zotero://open-pdf/library/items/276Q734B?page=3))_